<?php

namespace Drupal\drupal_moodle_integration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drupal_moodle_integration\Services\CourseService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/**
 * Defines CourseController class.
 */
class CourseController extends ControllerBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Moodle Course Service.
   *
   * @var \Drupal\drupal_moodle_integration\Services\CourseService
   */
  protected $courseService;

  /**
   * Construct for Course Controller.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param Drupal\drupal_moodle_integration\Services\CourseService $courseService
   *   Moodle course service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CourseService $courseService) {
    $this->entityTypeManager = $entity_type_manager;
    $this->courseService = $courseService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('drupal_moodle_integration.course_services'),
      $container->get('path.current')
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function coursesList() {
    $user = $this->currentUser();
    $moodle_id = $user->field_moodle_user_id->value;
    return [
      '#theme' => 'moodle_course_list',
      '#course_list' => $this->courseService->getCoursesList(),
      '#moodle_user_id' => $moodle_id,
      '#attached' => [
        'library' => [
          'drupal_moodle_integration/drupal_moodle_integration',
        ],
      ],
    ];
  }

  /**
   * Functoin to enroll course.
   */
  public function userEnrolledCourse() {
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser()->id());
    $moodle_id = $user->field_moodle_user_id->value;
    return [
      '#theme' => 'moodle_course',
      '#course' => $this->courseService->userAssignedcourses(),
      '#moodle_user_id' => $moodle_id,
      '#attached' => [
        'library' => [
          'drupal_moodle_integration/drupal_moodle_integration',
        ],
      ],
    ];
  }

  /**
   * Function to get list of activities form moodle.
   */
  public function courseGetActivities($courseid) {
    return [
      '#theme' => 'moodle_course_activity',
      '#course_activity' => $this->courseService->getActivities($courseid),
    ];
  }

  /**
   * Function to Unenrol.
   */
  public function courseUnenrol($userid, $courseid) {
    if ($userid != $this->currentUser()->id()) {
      throw new AccessDeniedException("You can only unenrol yourself.");
    }
    $this->courseService->courseUnEnrol($userid, $courseid);
    return $this->redirect('drupal_moodle_integration_course_activities.controller', ['courseid' => $courseid]);
  }

}
