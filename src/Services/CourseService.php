<?php

namespace Drupal\drupal_moodle_integration\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Class CourseService.
 */
class CourseService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user's account object.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new CourseService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AccountInterface $current_user, EntityTypeManagerInterface $entity_type_manager) {
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * User Courses List.
   */
  public function getCoursesList() {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_course_get_courses',
      'moodlewsrestformat' => 'json',
    ];
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    $newusers = json_decode($response);
    return $newusers;
  }

  /**
   * User Assigned Courses.
   */
  public function userAssignedcourses() {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $user = $this->entityTypeManager->getStorage('user')->load($this->currentUser->id());
    $moodle_id = $user->field_moodle_user_id->value;
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_enrol_get_users_courses',
      'moodlewsrestformat' => 'json',
      'userid' => $moodle_id,
    ];
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    $newusers = json_decode($response);
    return $newusers;
  }

  /**
   * Get All list of Activities.
   */
  public function getActivities($courseid) {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_course_get_contents',
      'moodlewsrestformat' => 'json',
    ];
    $params['courseid'] = $courseid;
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    $newusers = json_decode($response);
    return $newusers;
  }

  /**
   * Course Unenrol.
   */
  public function courseUnEnrol($userid, $courseid) {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'enrol_manual_unenrol_users',
      'moodlewsrestformat' => 'json',
    ];
    $params['enrolments'][0]['roleid'] = 5;
    $params['enrolments'][0]['userid'] = $userid;
    $params['enrolments'][0]['courseid'] = $courseid;
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    json_decode($response);
  }

}
