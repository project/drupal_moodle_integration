<?php

namespace Drupal\drupal_moodle_integration\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class UserService.
 */
class UserService {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new CourseService.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Check if Moodle connection is configured.
   */
  public function isConfigured() {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    return $config->get('url') && $config->get('wstoken');
  }

  /**
   * Function to get Create users In moodle.
   *
   * @param array $users
   *   An array of users to create to Moodle.
   */
  public function moodleCreateUser(array $users) {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_user_create_users',
      'moodlewsrestformat' => 'json',
      'users' => $users,
    ];
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    $newusers = json_decode($response);
    return $newusers[0]->id;
  }

  /**
   * Function to update users in moodle.
   *
   * @param array $users
   *   An array of users to update.
   */
  public function moodleUpdateUser(array $users) {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_user_update_users',
      'moodlewsrestformat' => 'json',
      'users' => $users,
    ];
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    json_decode($response);
  }

  /**
   * Get users.
   */
  public function getUsers() {
    $config = $this->configFactory->get('drupal_moodle_integration.settings');
    $baseurl = $config->get('url') . '/webservice/rest/server.php?';
    $params = [
      'wstoken' => $config->get('wstoken'),
      'wsfunction' => 'core_user_get_users',
      'moodlewsrestformat' => 'json',
      'criteria' => [
        ['key' => 'email', 'value' => '%%'],
      ],
    ];
    $url = $baseurl . http_build_query($params);
    $response = file_get_contents($url);
    $users = json_decode($response);
    return $users;
  }

}
