<?php

namespace Drupal\drupal_moodle_integration\Feeds\Fetcher\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a directory fetcher.
 */
class MoodleFetcherForm extends ExternalPluginFormBase implements ContainerInjectionInterface {

  /**
   * The config factory to use.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|null
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->configFactory = $container->get('config.factory');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api'] = [
      '#type' => 'select',
      '#title' => $this->t('Drupal Moodle Integration API'),
      '#required' => TRUE,
      '#options' => [
        'UserService' => [
          'getUsers' => $this->t('getUsers (core_user_get_users)'),
        ],
        // TODO: Follow-up issue.
        'CourseService' => [],
      ],
      '#description' => $this->t(
          'Moodle fetcher converts incoming data to csv format so that built-in Feeds parser plugins can be used to parse the data. See the <a href=":moodle_ws_api_documentation_url">Moodle WS API documentation</a> for available CSV column names.', [
            ':moodle_ws_api_documentation_url' => $this->configFactory->get('moodle.settings')->get('url') . '/admin/webservice/documentation.php',
          ]
      ),
      '#default_value' => $this->plugin->getConfiguration('api'),
    ];
    return $form;
  }

}
