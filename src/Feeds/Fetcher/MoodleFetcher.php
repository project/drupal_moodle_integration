<?php

namespace Drupal\drupal_moodle_integration\Feeds\Fetcher;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\FetcherResult;
use Drupal\feeds\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Moodle fetcher.
 *
 * @FeedsFetcher(
 *   id = "moodle",
 *   title = @Translation("Moodle"),
 *   description = @Translation("Uses Drupal Moodle Integration module API to fetch data from Moodle."),
 *   form = {
 *     "configuration" = "Drupal\drupal_moodle_integration\Feeds\Fetcher\Form\MoodleFetcherForm",
 *   }
 * )
 */
class MoodleFetcher extends PluginBase implements FetcherInterface, ContainerFactoryPluginInterface {

  /**
   * The user service.
   *
   * @var \Drupal\drupal_moodle_integration\Services\UserService
   */
  protected $userService;

  /**
   * The course service.
   *
   * @var \Drupal\drupal_moodle_integration\Services\CourseService
   */
  protected $courseService;

  /**
   * The config factory to use.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|null
   */
  protected $configFactory;

  /**
   * Drupal file system helper.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->userService = $container->get('drupal_moodle_integration.user_services');
    $instance->courseService = $container->get('drupal_moodle_integration.course_services');
    $instance->configFactory = $container->get('config.factory');
    $instance->fileSystem = $container->get('file_system');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function fetch(FeedInterface $feed, StateInterface $state) {
    $sink = $this->fileSystem->tempnam('temporary://', 'feeds_moodle_fetcher');
    $sink = $this->fileSystem->realpath($sink);
    switch ($this->getConfiguration()['api']) {
      case 'getUsers':
        $data = $this->userService->getUsers();
        $fileHandle = \fopen($sink, 'w');
        $writeHeader = TRUE;
        foreach ($data->users as $user) {
          if ($writeHeader) {
            $writeHeader = FALSE;
            \fputcsv($fileHandle, array_keys((array) $user));
          }
          \fputcsv($fileHandle, (array) $user);
        }
        \fclose($fileHandle);
        break;

      default:
        break;
    }
    return new FetcherResult($sink);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api' => NULL,
    ];
  }

}
